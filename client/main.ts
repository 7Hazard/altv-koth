import * as alt from "alt-client"
import * as native from "natives"

console.log("REQUESTING COLLISION AT (0 0 72)")
native.requestCollisionAtCoord(0, 0, 72)

let view = new alt.WebView("http://resource/client/index.html")
// let view = new alt.WebView("https://webglsamples.org/aquarium/aquarium.html")
view.isVisible = true
view.focus()
view.on("load", ()=>{
  console.log("THE WEBIEW HAS LOADED")
})

// alt.everyTick(()=>{

// })


native.requestModel(alt.hash("bkr_prop_coke_table01a"))
native.createObject(alt.hash("bkr_prop_coke_table01a"), 10, 10, 80, false, false, true)

native.requestModel(alt.hash("v_ilev_gasdoor"))
native.createObject(alt.hash("v_ilev_gasdoor"), 10, 10, 80, false, false, false)
native.createObject(alt.hash("v_ilev_gasdoor"), 11, 11, 81, false, false, false)
native.createObject(alt.hash("v_ilev_gasdoor"), 12, 12, 82, false, false, false)
native.createObject(alt.hash("v_ilev_gasdoor"), 13, 13, 83, false, false, false)
native.createObject(alt.hash("v_ilev_gasdoor"), 14, 14, 84, false, false, false)

