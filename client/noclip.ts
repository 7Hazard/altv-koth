import * as alt from "alt-client"

import {VK} from "./keys"

let up:boolean
let down:boolean
let left:boolean
let right:boolean

alt.on("keydown", (key) => {
  if(VK.KEY_W) up = true
  if(VK.KEY_S) down = true
  if(VK.KEY_A) left = true
  if(VK.KEY_D) right = true

  alt.emitServer("noclip:on", up || down || left || right)
})

alt.on("keyup", (key) => {
  if(VK.KEY_W) up = false
  if(VK.KEY_S) down = false
  if(VK.KEY_A) left = false
  if(VK.KEY_D) right = false

  alt.emitServer("noclip:off", up || down || left || right)
})
