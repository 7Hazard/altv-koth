package hazard7.alt.v.koth

import alt.v.kotlin.Log
import alt.v.kotlin.Resource
import alt.v.kotlin.entities.Player
import alt.v.kotlin.entities.Vehicle
import alt.v.kotlin.events.PlayerConnectEvent
import alt.v.kotlin.math.Float3

lateinit var resource: Resource

fun main(res: Resource)
{
    resource = res
    resource.clientType = "js"

//    Log.info("Max players: "+Core.maxPlayers)

//    var player: Player? = null
    resource.onPlayerConnect { ev ->
        val ply = ev.player
        Log.info("PLAYER ${ply.name} CONNECTED | REASON: ${ev.reason}")
        ply.health = 255
//        ply.pos = Float3(443f, 5572f, 798f)
        ply.pos = Float3(5f, 5f, 72f)

//        player = ply

        true
    }

    resource.onPlayerDisconnect { ev ->
        Log.info("PLAYER ${ev.player.name} DISCONNECTED | REASON: ${ev.reason}")
        true
    }

    resource.onPlayerDeath { ev ->
        val ply = ev.player
        val killer = ev.killer
        Log.info("PLAYER ${ply.name} was killed by ${killer.type} using ${ev.weapon}")
        ply.health = 255
        ply.pos = Float3(443f, 5572f, 798f)
        true
    }

    val veh = Vehicle("adder", Float3(10f, 10f, 72f), Float3(0f, 0f, 0f))

    resource.onTick {
        veh.pos = Float3(10f, 10f, 80f)
        veh.rot+=0.1f
//        Log.info("ROT: "+veh.rot.toString())
    }

    Log.info("King of the Hill loaded")
}
