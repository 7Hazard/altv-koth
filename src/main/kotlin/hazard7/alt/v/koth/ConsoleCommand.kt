package hazard7.alt.v.koth

import alt.v.kotlin.entities.Player

class ConsoleCommand constructor(
        kind:Kind,
        name:String,
        args:Array<Arg> = emptyArray(),
        handler: (player:Player, args:Array<Arg>) -> Unit
)
{
    enum class Kind
    {
        NoArgs,
        Args,
        Variadic
    }

    class Arg constructor(name:String, type:Type)
    {
        enum class Type
        {

        }

        val name = name
        val type = type
    }

    val kind = kind
    val name = name
    val args = args
    val handler = handler

//    constructor(name:String, handler:(player:Player, vararg args: Arg) -> Unit) : this(
//            Kind.NoArgs,
//            name,
//            emptyArray(),
//            handler
//    )
}
