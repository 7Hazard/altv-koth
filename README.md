# altV King of the Hill

Powered by Kotlin-JVM API: https://gitlab.com/7Hazard/altv-kotlin

King of the Hill is a gamemode where 3 teams battle for Mount Chilliad to claim the title of King of the Hill.

This gamemode was made to be used as an example resource for the Kotlin-JVM Server API for alt:V.
It was also made to stress test the server as a lot of events happen in the game.
